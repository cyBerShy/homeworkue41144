// HomeworkUE41144.cpp :
//14.4 ������������ ������
//��������� ������������ ���������� ������������ ������ ����������� ���������� std::string.

#include <iostream>
#include <string>
#include <windows.h> //����� ��� ����� ���������

class ConsoleCP
{
    int oldin;
    int oldout;

public:
    ConsoleCP(int cp)
    {
        oldin = GetConsoleCP();
        oldout = GetConsoleOutputCP();
        SetConsoleCP(cp);
        SetConsoleOutputCP(cp);
    }

    // ��������� �� �������� �������� �������� ������� � �������, ��� �����
    // ������� �� ��� ���� (���� ��������� �������, ������������ �� �������)
    ~ConsoleCP()
    {
        SetConsoleCP(oldin);
        SetConsoleOutputCP(oldout);
    }
};

int main()
{
    ConsoleCP cp(1251); //����������� ���� � ����� ������� �� ���������

    std::string TestString;
    std::cout << "������� ��������� ������: ";
    std::getline(std::cin, TestString);  //���� ������ � ����������
    std::cout << "+=====+" << "\n";
    std::cout << "��� ����� ����� ������--------[" << TestString << "]" << "\n";
    std::cout << "������ ���� ������ �����------[" << TestString.size() << "]" << "\n";
    std::cout << "������� ������ ���� ������----[" << TestString.substr(0, 1) << "]" << "\n";
    std::cout << "���������� ������ ���� ������-[" << TestString.substr(TestString.size() - 1, 1) << "]" << "\n";
    std::cout << "+====================================+" << "\n";
    return 0;

}
